#
# Be sure to run `pod lib lint RealTalk.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
s.name             = 'RealTalk'
s.version          = '0.0.14'
s.summary          = 'Framework providing helper functions and classes for networking/service calls.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

s.description      = <<-DESC
'Contains networking functions and protocols to centralize code that makes service/network calls. Each element needed in the service call will include a protocol (request, response). This way, we can have centralized functions that handle the elements abstractly. '
DESC

s.homepage         = 'https://bitbucket.org/mattpersonal/realtalk'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'mrrichardson52' => 'mattyrrich52@gmail.com' }
s.source           = { :git => 'https://mrrichardson52@bitbucket.org/mattpersonal/realtalk.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '13.1'

s.source_files = 'RealTalk/Sources/**/*'

s.swift_version= '4.2'

# s.resource_bundles = {
#   'RealTalk' => ['RealTalk/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'

end
