//
//  HttpMethod.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

public enum HttpMethod {
    case get
    case put
    case post
    case delete
    case patch
    case head
    case options
    case trace
    
    var name: String {
        switch self {
        case .get:
            return "GET"
            
        case .put:
            return "PUT"
            
        case .post:
            return "POST"
            
        case .delete:
            return "DELETE"
            
        case .head:
            return "HEAD"
            
        case .options:
            return "OPTIONS"
            
        case .trace:
            return "TRACE"
            
        case .patch:
            return "PATCH"
        }
    }
}
