//
//  HttpNetworkingStrategy.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
    
/// The different strategies that can be used to perform an http request call.
public enum HttpNetworkingStrategy {
    
    /// The networker will handle fetching the data using a URL Request
    case fetch
    
    /// The networker will use the mocked data instead of performing a URL Request
    case mock(data: Data, statusCode: Int)
}
