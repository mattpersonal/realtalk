//
//  HttpRequestBodyEncoding.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// The strategies used for encoding the http request body. 
public enum HttpRequestBodyEncoding {
    case json(encoder: JSONEncoder)
}
