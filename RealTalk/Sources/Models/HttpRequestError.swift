//
//  HttpRequestError.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// All possible errors that can be returned when performing an http request. 
public enum HttpRequestError<ErrorResponse>: Error {
    case serverError(statusCode: Int, errorResponse: ErrorResponse)
    case invalidUrlString(urlString: String?)
    case bodyEncodingError(encodingError: EncodingError)
    case urlError(URLError)
    case nonHttpUrlResponse
    case responseDecodingError(DecodingError)
    case emptyData
    case other(Error)
    
    public var localizedDescription: String {
        switch self {
        case let .invalidUrlString(urlString):
            return "Invalid Url String: \(urlString ?? "nil url string")"
            
        case let .bodyEncodingError(error):
            return "Error encoding body: \(error.localizedDescription)"
            
        case let .urlError(urlError):
            return "URL Error: \(urlError.localizedDescription)"
            
        case .nonHttpUrlResponse:
            return "Non HTTP URL Reponse returned"
            
        case let .serverError(statusCode, errorResponse):
            return "Server error with status code \(statusCode) and error response: \(errorResponse)"
            
        case let .responseDecodingError(error):
            return "Error decoding response: \(error.localizedDescription)"
            
        case .emptyData:
            return "No data was returned"
            
        case let .other(error):
            return "Unknown error: \(error.localizedDescription)"
        }
    }
}
