//
//  Networker.NetworkingResult.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// All possible networking results possible for http requests. 
public enum HttpRequestResult<Response, ErrorResponse> {
    case success(response: Response, statusCode: Int, headers: [String : String])
    case failure(error: HttpRequestError<ErrorResponse>)
}
