//
//  HttpResponseBodyDecoding.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// The strategies for decoding the http response body.
public enum HttpResponseBodyDecoding {
    case json(decoder: JSONDecoder)
}
