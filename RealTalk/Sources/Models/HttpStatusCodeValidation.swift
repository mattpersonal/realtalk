//
//  HttpStatusCodeValidation.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// The various strategies for validating the HTTP status code that gets returned in the HTTP Response.
public enum HttpStatusCodeValidation {
    
    /// Considers the returned status code a success if it is contained in the set of success codes.
    case successCodes(Set<Int>)
    
    /// Considers any status code a success. 
    case none
}
