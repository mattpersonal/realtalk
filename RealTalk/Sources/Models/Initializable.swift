//
//  Initializable.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// Any object that needs a default initializer. 
public protocol Initializable {
    
    /// Default initializer
    init()
}
