//
//  None.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// A class conforming to Codable that can be used as a Codable type in protocols with associated types. For example, the HttpRequest protocol includes an associatedtype for Response which must conform to Decodable. If the request does not even need the response, the protocol can specify None as the response type. 
public struct None: Codable, Initializable {
    public init() {}
}
