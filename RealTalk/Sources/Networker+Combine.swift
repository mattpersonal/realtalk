//
//  Networker+Combine.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/14/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

extension Networker {
    
    /// Creates and returns a publisher that executes an http request and produces an event when the response is received. This publisher produces a single event. On success, the publisher will send the parsed Resopnse object and then it will send the `finished` completion. On failure, the publisher will just send the `failure` completion along with the error that was encountered. 
    /// - Parameter httpRequest: request to execute
    /// - Parameter networkingStrategy: networking strategy for executing the request.
    public func publisher<Request: HttpRequest>(httpRequest: Request,
                                                networkingStrategy: HttpNetworkingStrategy) -> HttpRequestPublisher<Request> {
        return HttpRequestPublisher(networker: self,
                                    httpRequest: httpRequest,
                                    networkingStrategy: networkingStrategy)
    }
}

// MARK: - Subscription

extension Networker {
    
    final class HttpRequestSubscription<SubscriberType: Subscriber, Request: HttpRequest>: Subscription where SubscriberType.Input == Request.Response, SubscriberType.Failure == HttpRequestError<Request.ErrorResponse> {
        
        private weak var networker: Networker?
        private var subscriber: SubscriberType?
        private let request: Request
        private let networkingStrategy: HttpNetworkingStrategy
        private var requestCancellable: AnyCancellable?
        
        init(subscriber: SubscriberType, networker: Networker?, request: Request, networkingStrategy: HttpNetworkingStrategy) {
            self.subscriber = subscriber
            self.networker = networker
            self.request = request
            self.networkingStrategy = networkingStrategy
        }
        
        func request(_ demand: Subscribers.Demand) {
            guard demand != .none else {
                fatalError("Demand must not be zero.")
            }
            
            guard subscriber != nil, let networker = networker else {
                return
            }
            
            requestCancellable = networker.perform(httpRequest: request,
                                                   networkingStrategy: networkingStrategy,
                                                   completionQueue: .main,
                                                   completion: { [weak self] networkResult in
                                                    guard let strongSelf = self else {
                                                        return
                                                    }
                                                    
                                                    switch networkResult {
                                                    case let .success(response):
                                                        _ = strongSelf.subscriber?.receive(response)
                                                        strongSelf.subscriber?.receive(completion: .finished)
                                                        
                                                    case let .failure(httpRequestError):
                                                        strongSelf.subscriber?.receive(completion: .failure(httpRequestError))
                                                    }
                                                    
                                                    // do some cleanup
                                                    strongSelf.subscriber = nil
                                                    strongSelf.networker = nil
            })
        }
        
        func cancel() {
            subscriber = nil
        }
    }
}

// MARK: - Publisher

extension Networker {
    
    public struct HttpRequestPublisher<Request: HttpRequest>: Publisher {
        public typealias Output = Request.Response
        public typealias Failure = HttpRequestError<Request.ErrorResponse>
        
        private weak var networker: Networker?
        private let httpRequest: Request
        private let networkingStrategy: HttpNetworkingStrategy
        
        init(networker: Networker, httpRequest: Request, networkingStrategy: HttpNetworkingStrategy) {
            self.networker = networker
            self.httpRequest = httpRequest
            self.networkingStrategy = networkingStrategy
        }
        
        public func receive<S>(subscriber: S) where S : Subscriber, Failure == S.Failure, Output == S.Input {
            let subscription = HttpRequestSubscription(subscriber: subscriber,
                                                       networker: networker,
                                                       request: httpRequest,
                                                       networkingStrategy: networkingStrategy)
            subscriber.receive(subscription: subscription)
        }
    }
}
