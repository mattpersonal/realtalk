//
//  Networker.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import Combine

/// Networker is a singleton that handles performing networking calls.
public class Networker {
    
    /// Networker singleton
    public static let shared = Networker()
    
    /// Internal initializer (leaving internal and not private for potential unit testing purposes)
    init() {}
    
    public func perform<Request, Response, ErrorResponse>(httpRequest: Request,
                                                          networkingStrategy: HttpNetworkingStrategy,
                                                          completionQueue: DispatchQueue,
                                                          completion: @escaping (Result<Response, HttpRequestError<ErrorResponse>>) -> Void) -> AnyCancellable? where Request: HttpRequest, Request.Response == Response, Request.ErrorResponse == ErrorResponse {
        // create the URL from the string provided
        guard let urlString = httpRequest.urlString, let url = URL(string: urlString) else {
            completion(.failure(.invalidUrlString(urlString: httpRequest.urlString)))
            return nil
        }
        
        var urlRequest = URLRequest(url: url)
        
        // add the http method
        urlRequest.httpMethod = httpRequest.httpMethod.name
        
        // add the headers
        if let headers = httpRequest.headers {
            for header in headers {
                urlRequest.addValue(header.key, forHTTPHeaderField: header.value)
            }
        }
        
        // add the body
        if let body = httpRequest.body, Request.Body.self != None.self {
            
            // encoding the body
            let data: Data
            
            switch httpRequest.bodyEncoding {
            case let .json(encoder):
                do {
                    data = try body.encode(encoder: encoder)
                } catch let encodingError as EncodingError {
                    completion(.failure(.bodyEncodingError(encodingError: encodingError)))
                    return nil
                } catch {
                    completion(.failure(.other(error)))
                    return nil
                }
            }
            
            urlRequest.httpBody = data
        }
        
        // create the handler for the data and the status code produced from the call. This handler does not care if the data is mocked or not.
        let responseHandler = { (data: Data, statusCode: Int) -> Void in
            let statusCodeValidated: Bool
            switch httpRequest.statusCodeValidation {
            case let .successCodes(codes):
                statusCodeValidated = codes.contains(statusCode)
                
            case .none:
                statusCodeValidated = true
            }
            
            // make sure we passed validation
            guard statusCodeValidated else {
                // since validation failed, decode the error response
                let errorResponse: ErrorResponse
                
                if ErrorResponse.self == None.self || data.isEmpty {
                    errorResponse = ErrorResponse()
                } else {
                    switch httpRequest.responseDecoding {
                    case let .json(decoder):
                        do {
                            errorResponse = try decoder.decode(ErrorResponse.self, from: data)
                        } catch {
                            // On error decoding, just create an empty error response. Seems excessive to pass back errors encountered while decoding an error response. 
                            errorResponse = ErrorResponse()
                        }
                    }
                }
                
                completion(.failure(.serverError(statusCode: statusCode, errorResponse: errorResponse)))
                return
            }
            
            // decode the response
            let response: Response
            
            if Response.self == None.self || data.isEmpty {
                response = Response()
            } else {
                switch httpRequest.responseDecoding {
                case let .json(decoder):
                    do {
                        response = try decoder.decode(Response.self, from: data)
                    } catch let decodingError as DecodingError {
                        completion(.failure(.responseDecodingError(decodingError)))
                        return
                    } catch {
                        completion(.failure(.other(error)))
                        return
                    }
                }
            }
            
            // successfully retrieve the response
            completion(.success(response))
        }
        
        // get the data using the networking strategy
        switch networkingStrategy {
        case let .mock(data, statusCode):
            // use mocked data instead of performing any network calls
            responseHandler(data, statusCode)
            return nil
            
        case .fetch:
            // fetch data using the URL request
            let urlSession = URLSession(configuration: .default)
            
            return urlSession.dataTaskPublisher(for: urlRequest)
                .sink(receiveCompletion: { publisherCompletion in
                    switch publisherCompletion {
                    case .finished:
                        // do nothing. the data response and all would have been passed back via the receiveValue parameter.
                        break
                        
                    case let .failure(urlError):
                        completion(.failure(.urlError(urlError)))
                    }
                },
                      receiveValue: { (data, urlResponse) in
                        guard let httpResponse = urlResponse as? HTTPURLResponse else {
                            completion(.failure(.nonHttpUrlResponse))
                            return
                        }
                        
                        responseHandler(data, httpResponse.statusCode)
                })
        }
    }
}
