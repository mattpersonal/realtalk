//
//  HttpRequest.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

/// Protocol for any HTTP Request.
public protocol HttpRequest {
    
    /// The request body type that will be encoded into the http body data.
    /// - Note: If body is not needed, set to `None`
    associatedtype Body: Encodable
    
    /// The response type that the Data will be decoded into.
    /// - Note: If response is not needed, set to `None`
    associatedtype Response: Decodable & Initializable
    
    /// The error response type that the Data will be decoded into when an invalid status code is received.
    /// - Note: If error response is not needed, set to `None`.
    associatedtype ErrorResponse: Decodable & Initializable
    
    /// URL string for the request.
    var urlString: String? { get }
    
    /// The set of HTTP headers.
    var headers: [String : String]? { get }
    
    /// HTTP method for the request (GET, PUT, etc.)
    var httpMethod: HttpMethod { get }
    
    /// The encodable object that will be encoded into the http request body data.
    var body: Body? { get }
    
    /// The validation used to determine if the status code is a success or not.
    var statusCodeValidation: HttpStatusCodeValidation { get }
    
    /// The type specifying how the http request body should be decoded.
    var bodyEncoding: HttpRequestBodyEncoding { get }
    
    /// The type specifying how the responses should be decoded.
    var responseDecoding: HttpResponseBodyDecoding { get }
}

extension HttpRequest {    
    public var headers: [String : String]? {
        return nil
    }
    
    public var body: Body? {
        return nil
    }
    
    public var statusCodeValidation: HttpStatusCodeValidation {
        return .successCodes(Set(0 ... 299))
    }
    
    public var bodyEncoding: HttpRequestBodyEncoding {
        return .json(encoder: JSONEncoder())
    }
    
    public var responseDecoding: HttpResponseBodyDecoding {
        return .json(decoder: JSONDecoder())
    }
}
