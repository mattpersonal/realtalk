//
//  Encodable+Util.swift
//  RealTalk
//
//  Created by Matthew Richardson on 11/8/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation

extension Encodable {
    
    /// Encodes this encodable into data using the JSONEncoder. This function is needed since protocol types cannot conform to themselves. We use this function because the concrete type can be inferred using `self`. 
    /// - Parameter encoder: JSONEncoder to use for encoding.
    func encode(encoder: JSONEncoder) throws -> Data {
        return try encoder.encode(self)
    }
}
