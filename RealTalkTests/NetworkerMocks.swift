//
//  NetworkerMocks.swift
//  RealTalkTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import Foundation
import RealTalk

extension NetworkerTests {
    
    // MARK: - Requests
    
    struct MockRequest<Body, Response, ErrorResponse>: HttpRequest where Body: Encodable, Response: Decodable & Initializable, ErrorResponse: Decodable & Initializable {
        typealias Body = Body
        typealias Response = Response
        typealias ErrorResponse = ErrorResponse
        
        let urlString: String?
        let httpMethod: HttpMethod = .get
        let body: Body?
        let headers: [String : String]?
        let statusCodeValidation: HttpStatusCodeValidation
        
        init(body: Body? = nil,
             urlString: String? = "www.example.com",
             headers: [String : String]? = ["mockHeaderKey" : "mockHeaderValue"],
             statusCodeValidation: HttpStatusCodeValidation = .successCodes(Set(200 ... 299))) {
            self.body = body
            self.urlString = urlString
            self.headers = headers
            self.statusCodeValidation = statusCodeValidation
        }
    }
    
    // MARK: - Bodies
    
    struct MockBody: Encodable {
        let id: String?
        
        init() {
            id = "mock ID"
        }
    }
    
    struct MockBodyThrowingEncodingError: Encodable {
        func encode(to encoder: Encoder) throws {
            throw EncodingError.invalidValue("mock", EncodingError.Context(codingPath: [], debugDescription: "mock"))
        }
    }
    
    // MARK: - Responses
    
    struct MockSuccessResponse: Decodable, Initializable, Equatable {
        let name: String?
                
        init() {
            name = nil
        }
        
        init(name: String?) {
            self.name = name
        }
    }
    
    struct MockResponseThrowingDecodingError: Decodable, Initializable {
        init() {}
        
        init(from decoder: Decoder) throws {
            throw DecodingError.dataCorrupted(DecodingError.Context(codingPath: [], debugDescription: "mock"))
        }
    }
    
    struct MockErrorResponse: Decodable, Initializable, Equatable {
        let errorDescription: String?
        
        init() {
            errorDescription = nil
        }
        
        init(errorDescription: String?) {
            self.errorDescription = errorDescription
        }
    }
    
    // MARK: - Expected Values
    
    enum Expected {
        static let successResponse = MockSuccessResponse(name: Expected.successName)
        static let errorResponse = MockErrorResponse(errorDescription: Expected.errorResponseDescription)
        
        fileprivate static let successName = "mockName"
        fileprivate static let errorResponseDescription = "mockDescription"
    }
    
    // MARK: - Mock Data
    
    enum MockData {
        
        static let success = encode(json: MockData.successJson)
        static let errorResponse = encode(json: MockData.errorResponseJson)
        static let invalidJsonData = encode(json: MockData.invalidJson)
        static let empty = encode(json: MockData.emptyJson)
        
        private static let successJson = """
        {
            "name" : "\(Expected.successName)"
        }
        """
        
        private static let errorResponseJson = """
        {
            "errorDescription" : "\(Expected.errorResponseDescription)"
        }
        """
        
        private static let invalidJson = """
        {
            "errorDescription" : "\(Expected.errorResponseDescription)"
        }
        """
        
        private static let emptyJson = ""
        
        private static func encode(json: String) -> Data {
            return json.data(using: .utf8)!
        }
    }
}
