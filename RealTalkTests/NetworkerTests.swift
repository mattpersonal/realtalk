//
//  NetworkerTests.swift
//  RealTalkTests
//
//  Created by Matthew Richardson on 11/2/19.
//  Copyright © 2019 Matthew Richardson. All rights reserved.
//

import XCTest
import RealTalk
import Combine

class NetworkerTests: XCTestCase {
    
    private var sharedCancellable: AnyCancellable?
    
    override func tearDown() {
        sharedCancellable = nil
    }
    
    func test_networker_invalidUrlString() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>(urlString: "^^invalidurl^^^")
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case .invalidUrlString:
                                                break // success
                                                
                                            default:
                                                XCTFail("Expected different error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_bodyEncodingError() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBodyThrowingEncodingError, MockSuccessResponse, MockErrorResponse>(body: MockBodyThrowingEncodingError())
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case .bodyEncodingError:
                                                break // success
                                                
                                            default:
                                                XCTFail("Expected body encoding error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_bodyTypeNone_success() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<None, MockSuccessResponse, MockErrorResponse>(body: None())
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .success(response):
                                            XCTAssert(response.name == "mockName")
                                            
                                        case let .failure(error):
                                            XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_statusCodeValidation_none() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 401
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>(statusCodeValidation: .none)
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .success(response):
                                            XCTAssert(response.name == "mockName")
                                            
                                        case let .failure(error):
                                            XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_statusCodeValidation_customSuccessCodes() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 401
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>(statusCodeValidation: .successCodes(Set(400 ... 401)))
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .success(response):
                                            XCTAssert(response.name == "mockName")
                                            
                                        case let .failure(error):
                                            XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_serverError() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.errorResponse
        let mockStatusCode = 401
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case let .serverError(statusCode, errorResponse):
                                                XCTAssert(statusCode == mockStatusCode)
                                                XCTAssert(errorResponse == Expected.errorResponse)
                                                
                                            default:
                                                XCTFail("Expected other error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_serverError_errorResponseTypeNone() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.errorResponse
        let mockStatusCode = 401
        
        let request = MockRequest<MockBody, MockSuccessResponse, None>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case let .serverError(statusCode, _):
                                                XCTAssert(statusCode == mockStatusCode)
                                                
                                            default:
                                                XCTFail("Expected other error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_serverError_emptyResponse() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.empty
        let mockStatusCode = 401
        
        let request = MockRequest<MockBody, MockSuccessResponse, None>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case let .serverError(statusCode, _):
                                                XCTAssert(statusCode == mockStatusCode)
                                                
                                            default:
                                                XCTFail("Expected other error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_successResponse() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .success(response):
                                            XCTAssert(response == Expected.successResponse)
                                            
                                        case let .failure(error):
                                            XCTFail("Unexpected failure with error: \(error.localizedDescription)")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_responseDecodingError() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBody, MockResponseThrowingDecodingError, MockErrorResponse>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case let .failure(error):
                                            switch error {
                                            case .responseDecodingError:
                                                break // success
                                                
                                            default:
                                                XCTFail("Expected response decoding error")
                                            }
                                            
                                        case .success:
                                            XCTFail("Expected failure.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_responseTypeNone() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBody, None, MockErrorResponse>()
        
        _ = Networker.shared.perform(httpRequest: request,
                                     networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode),
                                     completionQueue: .main,
                                     completion: { networkResult in
                                        switch networkResult {
                                        case .success:
                                            break // success (the response is just type None)
                                            
                                        case .failure:
                                            XCTFail("Expected success.")
                                        }
                                        
                                        testExpecation.fulfill()
        })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
    
    func test_networker_success_publisher() {
        let testExpecation = expectation(description: "test expectation")
        
        let mockData = MockData.success
        let mockStatusCode = 200
        
        let request = MockRequest<MockBody, MockSuccessResponse, MockErrorResponse>()
        
        sharedCancellable = Networker.shared.publisher(httpRequest: request,
                                                       networkingStrategy: .mock(data: mockData, statusCode: mockStatusCode))
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished:
                    break // success
                    
                case .failure:
                    XCTFail("Unexpected failure")
                }
                
                testExpecation.fulfill()
            },
                  receiveValue: { response in
                    XCTAssert(response == Expected.successResponse)
            })
        
        let result = XCTWaiter().wait(for: [testExpecation], timeout: 3)
        XCTAssert(result == .completed, "expecation did not complete")
    }
}
